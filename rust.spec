# bootstrap from scratch, set the channel and date from src/stage0.json
%global bootstrap_version 1.82.0
%global bootstrap_channel 1.82.0
%global bootstrap_date 2024-10-17

# only the specified arch will use the bootstrap binary
# run spectool -g rust.spec to get the tarball.
#global bootstrap_arches x86_64 aarch64 
#ppc64le riscv64

%ifarch x86_64
%global extra_targets x86_64-unknown-none x86_64-unknown-uefi
%endif
%global all_targets %{?extra_targets}
%define target_enabled() %{lua:
  print(string.find(rpm.expand(" %{all_targets} "), rpm.expand(" %1 "), 1, true) or 0)
}

%global bundled_libgit2_version 1.8.1
%global min_llvm_version 18.0.0
%global bundled_llvm_version 19.1.1
%bcond_without bundled_llvm
%bcond_with llvm_static

%{lua: function rust_triple(arch)
  if arch == "ppc64le" then
    arch = "powerpc64le"
  elseif arch == "riscv64" then
    arch = "riscv64gc"
  end
  return arch.."-unknown-linux-gnu"
end}

%global rust_triple %{lua: print(rust_triple(rpm.expand("%{_target_cpu}")))}
%global rust_triple_env %{lua:
  print(string.upper(string.gsub(rpm.expand("%{rust_triple}"), "-", "_")))
}

%if %defined bootstrap_arches
# For each bootstrap arch, add an additional binary Source.
# Also define bootstrap_source just for the current target.
%{lua: do
  local bootstrap_arches = {}
  for arch in string.gmatch(rpm.expand("%{bootstrap_arches}"), "%S+") do
    table.insert(bootstrap_arches, arch)
  end
  local base = rpm.expand("https://static.rust-lang.org/dist/%{bootstrap_date}")
  local channel = rpm.expand("%{bootstrap_channel}")
  local target_arch = rpm.expand("%{_target_cpu}")
  for i, arch in ipairs(bootstrap_arches) do
    i = 1000 + i * 3
    local suffix = channel.."-"..rust_triple(arch)
    print(string.format("Source%d: %s/cargo-%s.tar.xz\n", i, base, suffix))
    print(string.format("Source%d: %s/rustc-%s.tar.xz\n", i+1, base, suffix))
    print(string.format("Source%d: %s/rust-std-%s.tar.xz\n", i+2, base, suffix))
    if arch == target_arch then
      rpm.define("bootstrap_source_cargo "..i)
      rpm.define("bootstrap_source_rustc "..i+1)
      rpm.define("bootstrap_source_std "..i+2)
      rpm.define("bootstrap_suffix "..suffix)
    end
  end
end}
%endif

%global _privatelibs lib(.*-[[:xdigit:]]{16}*|rustc.*)[.]so.*
%global __provides_exclude ^(%{_privatelibs})$
%global __requires_exclude ^(%{_privatelibs})$
%global __provides_exclude_from ^(%{_docdir}|%{rustlibdir}/src)/.*$
%global __requires_exclude_from ^(%{_docdir}|%{rustlibdir}/src)/.*$

%global _find_debuginfo_opts --keep-section .rustc

%global common_libdir %{_prefix}/lib
%global rustlibdir %{common_libdir}/rustlib

# Reduce rustc's own debuginfo and optimizations, no space left on device
# e.g. https://github.com/rust-lang/rust/issues/45854
%global reduced_debuginfo 1

%if 0%{?reduced_debuginfo}
%global enable_debuginfo --debuginfo-level=0 --debuginfo-level-std=2
%global enable_rust_opts --set rust.codegen-units-std=1
%define _lto_cflags %{nil} 
%bcond_with rustc_pgo
%else
# Build rustc with full debuginfo, CGU=1, ThinLTO, and PGO.
%global enable_debuginfo --debuginfo-level=2
%global enable_rust_opts --set rust.codegen-units=1 --set rust.lto=thin
%bcond_without rustc_pgo
%endif

# The distro flags are only appropriate for the host, not our cross-targets,
# and they're not as fine-grained as the default settings for std vs rustc.
%if %defined build_rustflags
%global build_rustflags %{nil}
%endif

# similar to __cflags_arch_* macros
%global rustc_target_cpus %{lua: do
  local ocs = tonumber(rpm.expand("0%{?ocs}"))
  local env =
    " RUSTC_TARGET_CPU_X86_64=x86-64" .. ((ocs <= 23) and "-v2" or "-v3")
    .. " RUSTC_TARGET_CPU_PPC64LE=" .. ((ocs >= 23) and "pwr9" or "pwr8")
  print(env)
end}

# Set up shared environment variables for build/install/check
# *_USE_PKG_CONFIG=1 convinces *-sys crates to use the system library.
%global rust_env %{shrink:
  %{?rustflags:RUSTFLAGS="%{rustflags}"}
  %{rustc_target_cpus}
  LIBSQLITE3_SYS_USE_PKG_CONFIG=1
  %{!?with_disabled_libssh2:LIBSSH2_SYS_USE_PKG_CONFIG=1}
}
%global export_rust_env export %{rust_env}

Summary:        The Rust Programming Language
Name:           rust
Version:        1.83.0
Release:        1%{?dist}
License:        (ASL 2.0 or MIT) and (BSD and MIT)
URL:            https://www.rust-lang.org
Source0:        https://static.rust-lang.org/dist/rustc-%{version}-src.tar.xz

Patch3000:      0001-Use-lld-provided-by-system.patch
Patch3001:      rustc-1.70.0-rust-gdb-substitute-path.patch

# override default target CPUs to match distro settings
Patch3002:      0001-Let-environment-variables-override-some-default-CPUs.patch
# not use the bundled library in libsqlite3-sys
Patch3003:      rustc-1.76.0-unbundle-sqlite.patch
# fix oom of compiler_builtins-0.1.123 in loongarch
Patch3004:      0001-fix-build-error-for-loongarch64.patch

%ifarch %{bootstrap_arches}
%global local_rust_root %{_builddir}/rust-%{bootstrap_suffix}
Provides:       bundled(%{name}-bootstrap) = %{bootstrap_version}
%else
BuildRequires:  (cargo >= %{bootstrap_version} with cargo <= %{version})
BuildRequires:  (%{name} >= %{bootstrap_version} with %{name} <= %{version})
%global local_rust_root %{_prefix}
%endif

BuildRequires:  make gcc gcc-c++
%if %with bundled_llvm
BuildRequires:  cmake >= 3.20.0 ninja-build
Provides:       bundled(llvm) = %{bundled_llvm_version}
%else
BuildRequires:  cmake >= 3.5.1
%if %defined llvm
%global llvm_root %{_libdir}/%{llvm}
%else
%global llvm llvm
%global llvm_root %{_prefix}
%endif
BuildRequires:  %{llvm}-devel >= %{min_llvm_version}
%if %with llvm_static
BuildRequires:  %{llvm}-static libffi-devel
%endif
%endif
BuildRequires:  ncurses-devel curl-devel
BuildRequires:  pkgconfig(libcurl) pkgconfig(liblzma)
BuildRequires:  pkgconfig(openssl) pkgconfig(zlib)
BuildRequires:  pkgconfig(sqlite3)
BuildRequires:  pkgconfig(libssh2)
BuildRequires:  python3 python3-rpm-macros
BuildRequires:  compiler-rt
BuildRequires:  procps-ng gdb glibc-static
Requires:       %{name}-std-static = %{version}-%{release}
Requires:       /usr/bin/cc
Provides:       rustc = %{version}-%{release}

%if %{without bundled_llvm}
%if "%{llvm_root}" == "%{_prefix}" || 0%{?scl:1}
%global llvm_has_filecheck 1
%endif
%endif

%description
Rust is a language empowering everyone to build reliable and efficient 
software. Rust is blazingly fast and memory-efficient: with no runtime 
or garbage collector, it can power performance-critical services, run 
on embedded devices, and easily integrate with other languages. Rust’s 
rich type system and ownership model guarantee memory-safety and thread-
safety.

This package provides the Rust compiler and documentation generator.

%package std-static
Summary:        Standard library for Rust
Provides:       %{name}-std-static-%{rust_triple} = %{version}-%{release}
Requires:       %{name} = %{version}-%{release}
Requires:       glibc-devel >= 2.17

%description std-static
This package provides the standard libraries for building applications
written in Rust.

%global target_package()                        \
%package std-static-%1                          \
Summary:        Standard library for Rust %1    \
Requires:       %{name} = %{version}-%{release}

%global target_description()                                            \
%description std-static-%1                                              \
This package includes the standard libraries for building applications  \
written in Rust for the %2 target %1.

%if %target_enabled x86_64-unknown-none
%target_package x86_64-unknown-none
Requires:       lld
%target_description x86_64-unknown-none embedded
%endif

%if %target_enabled x86_64-unknown-uefi
%target_package x86_64-unknown-uefi
Requires:       lld
%target_description x86_64-unknown-uefi embedded
%endif

%package debugger-common
Summary:        Common debugger pretty printers for Rust
BuildArch:      noarch

%description debugger-common
This package includes the common functionality for %{name}-gdb and %{name}-lldb.

%package gdb
Summary:        GDB pretty printers for Rust
Requires:       gdb
Requires:       %{name}-debugger-common = %{version}-%{release}
BuildArch:      noarch

%description gdb
This package provides the rust-gdb script, which allows easier debugging of Rust
programs.

%package lldb
Summary:        LLDB pretty printers for Rust
Requires:       lldb python3-lldb
Requires:       %{name}-debugger-common = %{version}-%{release}
BuildArch:      noarch

%description lldb
This package provides the rust-lldb script, which allows easier debugging of Rust
programs.

%package -n cargo
Summary:        Rust's package manager and build tool
BuildRequires:  git-core
Requires:       %{name}
Provides:       cargo-vendor = %{version}-%{release}
Provides:       bundled(libgit2) = %{bundled_libgit2_version}

%description -n cargo
This program is a package manager and build tool for the Rust language.

%package -n rustfmt
Summary:        A tool for formatting Rust code
Requires:       cargo
# /usr/bin/rustfmt links to rustc libs
Requires:       %{name} = %{version}-%{release}
Provides:       rustfmt-preview = %{version}-%{release}

%description -n rustfmt
A tool for formatting Rust code according to style guidelines.

%package analyzer
Summary:        Rust implementation of the Language Server Protocol
Requires:       %{name} = %{version}-%{release}
Recommends:     %{name}-src
 
# RLS is no longer available as of Rust 1.64, we recommend rust-analyzer.
Obsoletes:      rls < 1.64.0~
 
%description analyzer
This package privides an implementation of Language Server Protocol for the Rust programming 
language. It provides features like completion and goto definition for many code editors, 
including VS Code, Emacs and Vim.


%package -n clippy
Summary:        Lints to catch common mistakes and improve your Rust code
Requires:       cargo
Requires:       %{name} = %{version}-%{release}
Provides:       clippy-preview = %{version}-%{release}

%description -n clippy
A collection of lints to catch common mistakes and improve your Rust code.

%package src
Summary:        Sources for the Rust standard libraries
Recommends:    %{name}-std-static = %{version}-%{release}

BuildArch:      noarch

%description src
This package provides source files for the Rust standard libraries.

%prep

%ifarch %{bootstrap_arches}
rm -rf %{local_rust_root}
%setup -q -n cargo-%{bootstrap_suffix} -T -b %{bootstrap_source_cargo}
./install.sh --prefix=%{local_rust_root} --disable-ldconfig
%setup -q -n rustc-%{bootstrap_suffix} -T -b %{bootstrap_source_rustc}
./install.sh --prefix=%{local_rust_root} --disable-ldconfig
%setup -q -n rust-std-%{bootstrap_suffix} -T -b %{bootstrap_source_std}
./install.sh --prefix=%{local_rust_root} --disable-ldconfig
test -f '%{local_rust_root}/bin/cargo'
test -f '%{local_rust_root}/bin/rustc'
%endif

%autosetup -n rustc-%{version}-src -p1

sed -i '/^try python3 /i try %{__python3} "$@"' ./configure
sed -i "s|@BUILDDIR@|$PWD|" ./src/etc/rust-gdb
sed -i '/LZMA_API_STATIC/d' src/bootstrap/src/core/build_steps/tool.rs
%if %{without bundled_llvm} && %{with llvm_static}
# Static linking to distro LLVM needs -lffi, https://github.com/rust-lang/rust/issues/34486
sed -i.ffi -e '$a #[link(name = "ffi")] extern {}' compiler/rustc_llvm/src/lib.rs
%endif

%if %without bundled_llvm
rm -rf src/llvm-project/
mkdir -p src/llvm-project/libunwind/
%endif

# Remove submodules we don't need.
rm -rf src/gcc
rm -rf src/tools/enzyme
rm -rf src/tools/rustc-perf

# remove other unused vendored libs
rm -rf vendor/curl-sys*/curl/
rm -rf vendor/*jemalloc-sys*/jemalloc/
rm -rf vendor/libffi-sys*/libffi/
rm -rf vendor/libmimalloc-sys*/c_src/mimalloc/
rm -rf vendor/libsqlite3-sys*/{sqlite3,sqlcipher}/
rm -rf vendor/libssh2-sys*/libssh2/
rm -rf vendor/libz-sys*/src/zlib{,-ng}/
rm -rf vendor/lzma-sys*/xz-*/
rm -rf vendor/openssl-src*/openssl/

find vendor -name .cargo-checksum.json \
  -exec sed -i 's/"files":{[^}]*}/"files":{ }/' '{}' '+'

find -name '*.rs' -type f -perm /111 -exec chmod -v -x '{}' '+'

%build
%{export_rust_env}

# profiler path is version dependent, and uses LLVM-specific arch names in the filename
%define profiler %(echo %{_libdir}/clang/??/lib/libclang_rt.profile-*.a)
test -r "%{profiler}"

%configure --disable-option-checking \
  --docdir=%{_pkgdocdir} \
  --libdir=%{common_libdir} \
  --build=%{rust_triple} --host=%{rust_triple} --target=%{rust_triple} \
  --set target.%{rust_triple}.linker=%{__cc} \
  --set target.%{rust_triple}.cc=%{__cc} \
  --set target.%{rust_triple}.cxx=%{__cxx} \
  --set target.%{rust_triple}.ar=%{__ar} \
  --set target.%{rust_triple}.ranlib=%{_bindir}/ranlib \
  --set target.%{rust_triple}.profiler="%{profiler}" \
  %{?mingw_target_config} \
  --python=%{__python3} \
  --local-rust-root=%{local_rust_root} \
  --set build.rustfmt=/bin/true \
  %{!?with_bundled_llvm: --llvm-root=%{llvm_root} \
    %{!?llvm_has_filecheck: --disable-codegen-tests} \
    %{!?with_llvm_static: --enable-llvm-link-shared } } \
  --disable-llvm-static-stdcpp \
  --disable-llvm-bitcode-linker \
  --disable-lld \
  --disable-rpath \
  %{enable_debuginfo} \
  %{enable_rust_opts} \
  --set build.build-stage=2 \
  --set build.doc-stage=2 \
  --set build.install-stage=2 \
  --set build.test-stage=2 \
  --set build.optimized-compiler-builtins=false \
%if %with bundled_llvm
  --set llvm.optimize=true \
  --set llvm.assertions=false \
  --set llvm.cflags="-O2 -DNDEBUG" \
  --set llvm.cxxflags="-O2 -DNDEBUG" \
  --set llvm.ldflags="-Wl,-O2" \
%endif
  --enable-extended \
  --tools=cargo,clippy,rust-analyzer,rustfmt,src \
  --enable-vendor \
  --enable-verbose-tests \
  --dist-compression-formats=gz \
  --release-channel=stable \
  --release-description="%{?_vendor:%_vendor }%{version}-%{release}"

# At least 4GB per CPU for building rustc.
ncpus=$(/usr/bin/getconf _NPROCESSORS_ONLN)
%if %{with bundled_llvm}
max_cpus=4
%else
max_cpus=$(( ($(free -g | awk '/^Mem:/{print $2}') + 1) / 2 ))
%endif
if [ "$max_cpus" -ge 1 -a "$max_cpus" -lt "$ncpus" ]; then
  ncpus="$max_cpus"
fi

%if %with rustc_pgo
# Build the compiler with profile instrumentation
PROFRAW="$PWD/build/profiles"
PROFDATA="$PWD/build/rustc.profdata"
mkdir -p "$PROFRAW"
%{__python3} ./x.py build -j "$ncpus" sysroot --rust-profile-generate="$PROFRAW"
# Build cargo to generate compiler profiles
env LLVM_PROFILE_FILE="$PROFRAW/default_%%m_%%p.profraw" %{__python3} ./x.py --keep-stage=0 --keep-stage=1 build cargo
%{llvm_root}/bin/llvm-profdata merge -o "$PROFDATA" "$PROFRAW"
rm -r "$PROFRAW" build/%{rust_triple}/stage2*/
# Rebuild the compiler using the profile data  
%{__python3} ./x.py build -j "$ncpus" sysroot --rust-profile-use="$PROFDATA"
%endif

# Build the compiler normally (with or without PGO)
%{__python3} ./x.py build -j "$ncpus" sysroot

# Build everything else normally
%{__python3} ./x.py build
%{__python3} ./x.py doc

for triple in %{?all_targets} ; do
  %{__python3} ./x.py build --target=$triple std
done

%install
%{export_rust_env}

DESTDIR=%{buildroot} %{__python3} ./x.py install

for triple in %{?all_targets} ; do
  DESTDIR=%{buildroot} %{__python3} ./x.py install --target=$triple std
done

rm -rf ./build/dist/ ./build/tmp/

# some components duplicate-install bins, leaving backups we don't want
rm -f %{buildroot}%{_bindir}/*.old

%if "%{_libdir}" != "%{common_libdir}"
mkdir -p %{buildroot}%{_libdir}
find %{buildroot}%{common_libdir} -maxdepth 1 -type f -name '*.so' \
  -exec mv -v -t %{buildroot}%{_libdir} '{}' '+'
%endif

find %{buildroot}%{_libdir} -maxdepth 1 -type f -name '*.so' \
  -exec chmod -v +x '{}' '+'

find %{buildroot}%{rustlibdir} -maxdepth 1 -type f -exec rm -v '{}' '+'
find %{buildroot}%{rustlibdir} -type f -name '*.orig' -exec rm -v '{}' '+'
find %{buildroot}%{rustlibdir}/src -type f -name '*.py' -exec rm -v '{}' '+'
rm -f %{buildroot}%{_pkgdocdir}/README.md
rm -f %{buildroot}%{_pkgdocdir}/COPYRIGHT
rm -f %{buildroot}%{_pkgdocdir}/LICENSE
rm -f %{buildroot}%{_pkgdocdir}/LICENSE-APACHE
rm -f %{buildroot}%{_pkgdocdir}/LICENSE-MIT
rm -f %{buildroot}%{_pkgdocdir}/LICENSE-THIRD-PARTY
rm -f %{buildroot}%{_pkgdocdir}/*.old
find %{buildroot}%{_pkgdocdir}/html -empty -delete
find %{buildroot}%{_pkgdocdir}/html -type f -exec chmod -x '{}' '+'
mkdir -p %{buildroot}%{_datadir}/cargo/registry
rm -f %{buildroot}%{rustlibdir}/%{rust_triple}/bin/rust-ll*

%check
%{export_rust_env}

TMP_HELLO=$(mktemp -d)
(
  cd "$TMP_HELLO"
  export RUSTC=%{buildroot}%{_bindir}/rustc \
    LD_LIBRARY_PATH="%{buildroot}%{_libdir}:$LD_LIBRARY_PATH"
  %{buildroot}%{_bindir}/cargo init --name hello-world
  %{buildroot}%{_bindir}/cargo run --verbose

  # Sanity-check that code-coverage builds and runs
  env RUSTFLAGS="-Cinstrument-coverage" %{buildroot}%{_bindir}/cargo run --verbose
  test -r default_*.profraw

  # Try a build sanity-check for other std-enabled targets
  for triple in %{?mingw_targets}; do
    %{buildroot}%{_bindir}/cargo build --verbose --target=$triple
  done
)
rm -rf "$TMP_HELLO"

# Bootstrap is excluded because a lot of its tests are geared toward the upstream CI.
%{__python3} ./x.py test --no-fail-fast --skip={src/bootstrap,tests/crashes} || :
rm -rf "./build/%{rust_triple}/test/"

%ifarch aarch64            
# see https://github.com/rust-lang/rust/issues/123733            
%define cargo_test_skip --test-args "--skip panic_abort_doc_tests"            
%endif
%{__python3} ./x.py test --no-fail-fast cargo %{?cargo_test_skip} || :
rm -rf "./build/%{rust_triple}/stage2-tools/%{rust_triple}/cit/"

%{__python3} ./x.py test --no-fail-fast clippy || :

%{__python3} ./x.py test --no-fail-fast rust-analyzer || :

%{__python3} ./x.py test --no-fail-fast rustfmt || :

%files
%license COPYRIGHT LICENSE-APACHE LICENSE-MIT
%doc README.md
%dir %{_pkgdocdir}
%{_pkgdocdir}/html
%{_bindir}/rustc
%{_bindir}/rustdoc
%{_libdir}/*.so
%{_libexecdir}/rust-analyzer-proc-macro-srv
%{_mandir}/man1/rustc.1*
%{_mandir}/man1/rustdoc.1*

%files std-static
%dir %{rustlibdir}
%dir %{rustlibdir}/%{rust_triple}
%dir %{rustlibdir}/%{rust_triple}/lib
%{rustlibdir}/%{rust_triple}/lib/*.rlib
# exclude target shared libraries, the compiler is linking `std` statically
%exclude %{rustlibdir}/%{rust_triple}/lib/*.so

%global target_files()      \
%files std-static-%1        \
%dir %{rustlibdir}          \
%dir %{rustlibdir}/%1       \
%dir %{rustlibdir}/%1/lib   \
%{rustlibdir}/%1/lib/*.rlib

%if %target_enabled x86_64-unknown-none
%target_files x86_64-unknown-none
%endif

%if %target_enabled x86_64-unknown-uefi
%target_files x86_64-unknown-uefi
%endif

%files debugger-common
%dir %{rustlibdir}
%dir %{rustlibdir}/etc
%{rustlibdir}/etc/rust_*.py*

%files gdb
%{rustlibdir}/etc/gdb_*
%{_bindir}/rust-gdb
%exclude %{_bindir}/rust-gdbgui

%files lldb
%{rustlibdir}/etc/lldb_*
%{_bindir}/rust-lldb

%files -n cargo
%license src/tools/cargo/LICENSE-APACHE src/tools/cargo/LICENSE-MIT src/tools/cargo/LICENSE-THIRD-PARTY
%doc src/tools/cargo/README.md
%{_sysconfdir}/bash_completion.d/cargo
%{_datadir}/zsh/site-functions/_cargo
%dir %{_datadir}/cargo
%dir %{_datadir}/cargo/registry
%{_bindir}/cargo
%{_mandir}/man1/cargo*.1*

%files -n rustfmt
%license src/tools/rustfmt/LICENSE-{APACHE,MIT}
%doc src/tools/rustfmt/{README,CHANGELOG,Configurations}.md
%{_bindir}/rustfmt
%{_bindir}/cargo-fmt

%files analyzer
%license src/tools/rust-analyzer/LICENSE-{APACHE,MIT}
%doc src/tools/rust-analyzer/README.md
%{_bindir}/rust-analyzer


%files -n clippy
%license src/tools/clippy/LICENSE-{APACHE,MIT}
%doc src/tools/clippy/{README.md,CHANGELOG.md}
%{_bindir}/cargo-clippy
%{_bindir}/clippy-driver

%files src
%dir %{rustlibdir}
%{rustlibdir}/src

%changelog
* Mon Dec 30 2024 Wang Guodong <gordonwwang@tencent.com> - 1.83.0-1
- Upgrade to 1.83.0
- enable bundled llvm

* Fri Dec 27 2024 Wang Guodong <gordonwwang@tencent.com> - 1.82.0-1
- Upgrade to 1.82.0
- fix build error for loongarch64

* Tue Dec 24 2024 Wang Guodong <gordonwwang@tencent.com> - 1.81.0-2
- Disable thin-LTO and PGO for rustc, lack of memory

* Mon Dec 23 2024 Wang Guodong <gordonwwang@tencent.com> - 1.81.0-1
- Upgrade to 1.81.0

* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.80.0-3
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.80.0-2
- Rebuilt for loongarch release

* Mon Aug 12 2024 Wang Guodong <gordonwwang@tencent.com> - 1.80.0-1
- Upgrade to 1.80.0

* Wed Jul 24 2024 Wang Guodong <gordonwwang@tencent.com> - 1.79.0-1
- Upgrade to 1.79.0
- no longer conflicts with rustup

* Mon Jul 22 2024 Wang Guodong <gordonwwang@tencent.com> - 1.78.0-1
- Upgrade to 1.78.0

* Tue Jul 9 2024 Wang Guodong <gordonwwang@tencent.com> - 1.77.2-1
- Upgrade to 1.77.2

* Fri Jun 21 2024 Wang Guodong <gordonwwang@tencent.com> - 1.76.0-1
- Upgrade to 1.76.0
- Enable thin-LTO and PGO for rustc
- unbundle sqlite

* Sun Feb 18 2024 Wang Guodong <gordonwwang@tencent.com> - 1.75.0-2
- Fix CVEs: CVE-2024-24575, CVE-2024-24577

* Wed Jan 17 2024 Wang Guodong <gordonwwang@tencent.com> - 1.75.0-1
- Upgrade to 1.75.0

* Wed Dec 20 2023 Wang Guodong <gordonwwang@tencent.com> - 1.74.1-1
- Upgrade to 1.74.1

* Fri Dec 15 2023 Wang Guodong <gordonwwang@tencent.com> - 1.74.0-1
- Upgrade to 1.74.0
- Compile based on llvm 17

* Fri Oct 20 2023 Wang Guodong <gordonwwang@tencent.com> - 1.73.0-1
- Upgrade to 1.73.0
- Add build target for x86_64-unknown-none, x86_64-unknown-uefi
- Use emmalloc instead of CC0 dlmalloc when bundling wasi-libc

* Fri Oct 13 2023 Wang Guodong <gordonwwang@tencent.com> - 1.72.0-1
- Upgrade to 1.72.0
- fix CVEs: CVE-2023-38497, CVE-2023-40030

* Thu Oct 12 2023 Miaojun Dong <zoedong@tencent.com> - 1.71.0-3
- Rebuild for curl-8.4.0

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.71.0-2
- Rebuilt for OpenCloudOS Stream 23.09

* Tue Jul 25 2023 Wang Guodong <gordonwwang@tencent.com> - 1.71.0-1
- Upgrade to 1.71.0

* Tue Jul 25 2023 Wang Guodong <gordonwwang@tencent.com> - 1.70.0-1
- Upgrade to 1.70.0
- Set the rustup proxy package and rustup as Conflicts

* Thu Jul 20 2023 Wang Guodong <gordonwwang@tencent.com> - 1.69.0-3
- disable bootstrap_arches

* Wed Jul 19 2023 Wang Guodong <gordonwwang@tencent.com> - 1.69.0-2
- Update for llvm 16

* Fri Jul 14 2023 Wang Guodong <gordonwwang@tencent.com> - 1.69.0-1
- Upgrade to 1.69.0

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.68.2-2
- Rebuilt for OpenCloudOS Stream 23.05

* Wed Apr 26 2023 Wang Guodong <gordonwwang@tencent.com> - 1.68.2-1
- Update to 1.68.2
- Provides rust-std-static with the target triple virtually.

* Tue Apr 25 2023 Wang Guodong <gordonwwang@tencent.com> - 1.67.1-1
- Update to 1.67.1

* Tue Apr 25 2023 Wang Guodong <gordonwwang@tencent.com> - 1.66.0-1
- Update to 1.66.0

* Mon Apr 24 2023 Wang Guodong <gordonwwang@tencent.com> - 1.65.0-1
- Update to 1.65.0
- analysis directories and files are no longer provided.

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.64.0-3
- Rebuilt for OpenCloudOS Stream 23

* Tue Dec 27 2022 Wang Guodong <gordonwwang@tencent.com> - 1.64.0-2
- Remove /usr/bin/rls from the ra package.

* Mon Dec 5 2022 Wang Guodong <gordonwwang@tencent.com> - 1.64.0-1
- Update to 1.64.0.
- Add rust-analyzer(ra), ra obsoletes rls.

* Mon Aug 15 2022 Xiaojie Chen <jackxjchen@tencent.com> - 1.63.0-1
- Initial build
